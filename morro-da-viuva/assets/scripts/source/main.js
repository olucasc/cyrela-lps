(function($) {

	/* Functions
	------------------------ */

	//Detectar usuário de teclado
	function handleFirstTab(e) {
	    if (e.keyCode === 9) { // detecta o primeiro tab do usuário
	    	document.body.classList.add('user-is-tabbing');
	    	window.removeEventListener('keydown', handleFirstTab);
	    }
	}
	// Play Video
	function playVideo( $el ) {

		var videoCode = $el.attr( 'data-id' ),
		player;

		if( typeof( YT ) !== 'undefined' ) {

			$el.addClass( 'play' );
			player = new YT.Player( videoCode, {
				videoId: videoCode,
				playerVars: {
					rel: 0,
					autoplay: 1,
					enablejsapi: 1
				}
			});

		}

	}

	//Funções Tags

	function standardPush(ec, ea, el){
		dataLayer.push({ 'event':'clickEvent', 'eventCategory':ec, 'eventAction':ea, 'eventLabel':el});
	}

	//Fecha Funções Tags

	//Funções aux Tags

	function retira_acentos(palavra) { 
	    com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ'; 
	    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC'; 
	    nova=''; 
	    for(i=0;i<palavra.length;i++) { 
	        if (com_acento.search(palavra.substr(i,1))>=0) { 
	            nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1); 
	        } 
	        else { 
	            nova+=palavra.substr(i,1); 
	        } 
	    } 
	    return nova; 
	}

	/* Triggers
	------------------------ */
	// Document Ready
	$(document).ready( function() {

		//Galeria de vistas
		$('.site-view_gallery').slick({
			dots: false,
			infinite: false,
			speed: 500,
        });

        //Galeria de Imagens
        $('.site-gallery_pics').slick({
            dots: false,
            infinite: false,
            speed: 500,
        });

		//Simulador
		$('.abrir-simulador').on('click', function(){
			$('#preco-simulador').removeClass('hidden');
		})

		//Tag de carregamento
		dataLayer.push({
			'event':'productImpressions',
			'eventCategory':'cyrela-lp:morro-da-viuva-rj',
			'eventAction':'product-impressions',
			'eventLabel':'product-impressions',
			'ecommerce':{
				'currencyCode':'BRL',
				'impressions':[
					{
						'name':'morro-da-viuva',
						'id':'2179',
						'price':'sob-consulta',
						'category':'rj',
						'list':'lp/rj/morro-da-viuva-rj',
						'position':1
					}
				]
			}
		});
		//Tags Footer
			//Tag Social
			$('.social a').on('click', function(){
				var ec = 'cyrela-lp:morro-da-viuva-rj';
				var el = $(this).data("social");
				var ea = 'social:'+el+':click';
				var el = 'social:'+el;
				standardPush(ec, ea, el);
			})

			//Tag Site
			$('.cilo .esquerda ul li a, .canais-de-contato .esquerda li a').on('click', function(){
				var ec = 'cyrela-lp:morro-da-viuva-rj';
				var el = $(this).parent().parent().data("list")+":"+$(this).attr("title");
				var el = retira_acentos(el.toLowerCase().replace(/ /g, "-"));
				var ea = el+':click'
				standardPush(ec, ea, el);
			})

		//Fecha Tags Footer

	});

	//Trigger de usuário de teclado
	window.addEventListener('keydown', handleFirstTab);

	//Tag Barra Fixa
	$(document).on('click', '.site-nav a', function() {
		if($(this).data("ponto") !== "telefone" && $(this).data("ponto") !== "simulador"){
			event.preventDefault();
		}
		var endereco = $(this).attr("href");
		var ponto = $(this).data("ponto");
		var ea = 'barra-fixa:'+ponto+':click';
		var el = 'barra-fixa:'+ponto;
		dataLayer.push({ 'event':'clickEvent', 'eventCategory':'cyrela-lp:morro-da-viuva-rj', 'eventAction':ea, 'eventLabel':el });
		
		if($(this).data("ponto") !== "telefone" && $(this).data("ponto") !== "simulador"){
			// definindo as medidas
	  		var alturaJanela = $(window).height();
	  		var larguraJanela = $(window).width();
	  		var topPop = (alturaJanela - 400) / 2;
	  		var leftPop = (larguraJanela - 200) / 2;

			window.open(endereco, "_blank","toolbar=no,scrollbars=yes,resizable=yes,top=150,left=300,width=700,height=400");
		}
	})

	//Tag Pontos Contato
	$(document).on('click', '.section-contato__item a', function() {
		if($(this).data("ponto") !== "telefone"){
			event.preventDefault();
		}
		var endereco = $(this).attr("href");
		var ponto = $(this).data("ponto");
		var ea = 'pontos-de-contato:'+ponto+':click';
		var el = 'pontos-de-contato:'+ponto;
		dataLayer.push({ 'event':'clickEvent', 'eventCategory':'cyrela-lp:morro-da-viuva-rj', 'eventAction':ea, 'eventLabel':el });
		
		if($(this).data("ponto") !== "telefone"){
			// definindo as medidas
	  		var alturaJanela = $(window).height();
	  		var larguraJanela = $(window).width();
	  		var topPop = (alturaJanela - 400) / 2;
	  		var leftPop = (larguraJanela - 200) / 2;

			window.open(endereco, "_blank","toolbar=no,scrollbars=yes,resizable=yes,top=150,left=300,width=700,height=400");
		}
	})

	//Tag Galeria
	$(document).on("click", ".slick-arrow", function(){
		var $this = $(this);
		setTimeout (function(){
			var tipoAcao, nomeImagem;
			
			if ($this.hasClass('slick-prev')) {
				tipoAcao = 'previous';
			}
			else if ($this.hasClass('slick-next')) {
				tipoAcao = 'next';
			}		
			var nome = $this.parents('.slick-slider').find('.slick-current p').html();
			console.log(nome);
			if (nome !== undefined){
				nome = nome.toLowerCase().replace(/ /g, "-");				
				nome=retira_acentos(nome);
				var el = tipoAcao+':'+nome
				dataLayer.push({ 'event':'clickEvent', 'eventCategory':'cyrela-lp:vintage-senior-residence-rs', 'eventAction':'carousel:click', 'eventLabel':el });
			}

		}, 1000);
	});

})(jQuery);